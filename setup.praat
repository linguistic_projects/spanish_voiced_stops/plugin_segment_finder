# Copyright 2017 Rolando Muñoz

Add menu command: "Objects", "Goodies", "Segment finder", "", 0, ""

# Main command
Add menu command: "Objects", "Goodies", "Run plug-in...", "Segment finder", 1, "scripts/finder/finder_form.praat"
Add menu command: "Objects", "Goodies", "-", "Segment finder", 1, ""

# Index
Add menu command: "Objects", "Goodies", "Index", "Segment finder", 1, ""
Add menu command: "Objects", "Goodies", "Scan TextGrids...", "Index", 2, "scripts/index/index_by_phones_form.praat"
Add menu command: "Objects", "Goodies", "-", "Index", 2, ""

## Index > Import table
Add menu command: "Objects", "Goodies", "Import", "Index", 2, ""
Add menu command: "Objects", "Goodies", "Index table...", "Import", 3, "scripts/settings/index_table_import.praat"
Add menu command: "Objects", "Goodies", "Targets table...", "Import", 3, "scripts/settings/target_table_import.praat"
Add menu command: "Objects", "Goodies", "Speakers table...", "Import", 3, "scripts/settings/speaker_table_import.praat"

## Index > Export table
Add menu command: "Objects", "Goodies", "Export", "Index", 2, ""
Add menu command: "Objects", "Goodies", "Index table", "Export", 3, "scripts/settings/index_table_export.praat"
Add menu command: "Objects", "Goodies", "Targets table", "Export", 3, "scripts/settings/target_table_export.praat"
Add menu command: "Objects", "Goodies", "Speakers table", "Export", 3, "scripts/settings/speaker_table_export.praat"

# Preferences
Add menu command: "Objects", "Goodies", "-", "Index", 2, ""
Add menu command: "Objects", "Goodies", "Preferences", "Segment finder", 1, ""
Add menu command: "Objects", "Goodies", "Set working directory...", "Preferences", 2, "scripts/settings/settings_working_directory.praat"
Add menu command: "Objects", "Goodies", "Set transcriber ID...", "Preferences", 2, "scripts/settings/settings_transcriber_id.praat"
Add menu command: "Objects", "Goodies", "Set TextGrid tiers...", "Preferences", 2, "scripts/settings/settings_tg.praat"
Add menu command: "Objects", "Goodies", "-", "Segment finder", 1, ""
Add menu command: "Objects", "Goodies", "About", "Segment finder", 1, "./scripts/about.praat"
