# Copyright 2017 Rolando Muñoz

include ../../procedures/config.proc

@config.init: "../../preferences/logfile.txt"

fileName$ = chooseReadFile$: "Sept 1: Open a tab separated file"
if fileName$ <> ""
  tb = Read Table from tab-separated file: fileName$
else
  exitScript()
endif

if Object_'tb'.ncol < 6
  exitScript: "This table must have 6 columns at least"
endif

beginPause: "step 2"
  comment: "Match the column names"
  optionMenu: "Key column", 1
  for i to Object_'tb'.ncol
    option: Object_'tb'.col$[i]
  endfor
  optionMenu: "Phoneme", 2
  for i to Object_'tb'.ncol
    option: Object_'tb'.col$[i]
  endfor
  optionMenu: "Word", 4
  for i to Object_'tb'.ncol
    option: Object_'tb'.col$[i]
  endfor
  optionMenu: "Code", 6
  for i to Object_'tb'.ncol
    option: Object_'tb'.col$[i]
  endfor
  optionMenu: "Phoneme occurrence", 3
  for i to Object_'tb'.ncol
    option: Object_'tb'.col$[i]
  endfor
  optionMenu: "Word occurrence", 5
  for i to Object_'tb'.ncol
    option: Object_'tb'.col$[i]
  endfor
clicked = endPause: "Continue", "Quit", 1

tb_tmp = Create Table without column names: "table", 0, 6
col# = {key_column, phoneme, word, code, phoneme_occurrence, word_occurrence}
for i to 6
  col = col# [i]
  column_index = Get column index: Object_'tb'.col$[col]
  if column_index
    exitScript: "A duplicate column """ + Object_'tb'.col$[col] + """ has been found"
  endif
  Set column label (index): i, Object_'tb'.col$[col]
endfor

if clicked = 2
  exitScript()
endif

selectObject: tb
Save as text file: "../../preferences/info_targets.Table"
@config.setField: "targets_table.key", Object_'tb'.col$[key_column]
@config.setField: "targets_table.phoneme", Object_'tb'.col$[phoneme]
@config.setField: "targets_table.word", Object_'tb'.col$[word]
@config.setField: "targets_table.code", Object_'tb'.col$[code]
@config.setField: "targets_table.phoneme.occurrence", Object_'tb'.col$[phoneme_occurrence]
@config.setField: "targets_table.word.occurrence", Object_'tb'.col$[word_occurrence]
removeObject: tb, tb_tmp

pauseScript: "Completed successfully"
