# Copyright 2017 Rolando Muñoz

include ../../procedures/config.proc

@config.init: "../../preferences/logfile.txt"

beginPause: "Segment finder - Preferences"
  sentence: "TextGrid folder directory", config.init.return$["tg_folder.dir"]
  sentence: "Sound folder directory", config.init.return$["sd_folder.dir"]
clicked = endPause: "Continue", "Quit", 1

if clicked = 1
  @config.setField: "tg_folder.dir", textGrid_folder_directory$
  @config.setField: "sd_folder.dir", sound_folder_directory$
endif
