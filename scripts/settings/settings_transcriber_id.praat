# Copyright 2017 Rolando Muñoz

include ../../procedures/config.proc

@config.init: "../../preferences/logfile.txt"

beginPause: "Segment finder - Preferences"
  sentence: "Transcriber ID", config.init.return$["transcriber_id"]
clicked = endPause: "Continue", "Quit", 1

if clicked = 1
  @config.setField: "transcriber_id", transcriber_ID$
endif
