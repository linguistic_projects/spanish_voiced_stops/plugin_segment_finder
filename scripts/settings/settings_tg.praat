# Copyright 2017 Rolando Muñoz

include ../../procedures/config.proc

@config.init: "../../preferences/logfile.txt"

beginPause: "set TextGrid tiers"
  word: "Tier phoneme", config.init.return$["tg_tier.phoneme"]
  word: "Tier word", config.init.return$["tg_tier.word"]
  word: "Tier code", config.init.return$["tg_tier.code"]
clicked = endPause: "Continue", "Quit", 1

if clicked = 2
  exitScript()
endif

@config.setField: "tg_tier.phoneme", tier_phoneme$
@config.setField: "tg_tier.word", tier_word$
@config.setField: "tg_tier.code", tier_code$
