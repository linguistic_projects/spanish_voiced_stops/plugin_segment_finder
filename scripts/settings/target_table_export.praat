# Copyright 2017 Rolando Muñoz

dir$ = "../../preferences/info_targets.Table"

if fileReadable(dir$)
  tb = Read from file: dir$
else
    exitScript: "The Target table has not been found"
endif
