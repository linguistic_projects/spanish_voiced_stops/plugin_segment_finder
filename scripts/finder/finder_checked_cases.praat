# Copyright 2017 Rolando Muñoz

include ./../../procedures/config.proc

form Interval finder
  comment TextGrid info
  natural Segment_tier 1
  comment Segmental view
  real margin(s) 0.3
  comment Spectrogram settings
  real Dynamic_range 45
  real Max_range(Hz) 5500
endform

# Read local variables

@config.init: "../../preferences/logfile.txt"

sd_folder$ = config.init.return$["sd_folder.dir"]
tg_folder$ = config.init.return$["tg_folder.dir"]
index_row = number(config.init.return$["finder_case"])
transcriptorID$ = config.init.return$["transcriber_id"]

# Open index table
index = Read from file: "../../preferences/index.Table"
number_of_rows = Object_'index'.nrow
break = 1

filename_col$ = "filename"
tmin_col$ = "tmin"
tmax_col$ = "tmax"
tmid_col$ = "tmid"
status_col$ = "status"
intensity_max_col$ = "intensity_max"

# Open the the Table index and go along each row

while break
  ## Set case number
  @config.setField: "finder_case", string$(index_row)

  ## Get row values from index
  tg_filename$ = object$ [index, index_row, filename_col$] + ".TextGrid"
  sd_filename$ = object$ [index, index_row, filename_col$] + ".wav"
  status = object[index, index_row, status_col$]
  status$ = if status = 1 then "done" else "missing" fi
  tmin = object[index, index_row, tmin_col$]
  tmax = object[index, index_row, tmax_col$]
  tmid = object[index, index_row, tmid_col$]
  intensity_max = object[index, index_row, intensity_max_col$]

  ## Open files
  if not fileReadable(tg_folder$ + "/" + tg_filename$) or not fileReadable(sd_folder$ + "/" + sd_filename$)
    filename$ = if fileReadable(tg_folder$ + "/" + tg_filename$) then tg_filename$ else sd_filename$ fi
    exitScript: filename$ + "does not exist in the origin folder"
  endif
  
  tg = Read from file: tg_folder$ + "/" + tg_filename$
  sd = Read from file: sd_folder$ + "/" + sd_filename$

  selectObject: tg
  plusObject: sd
  View & Edit
  
  editor: tg
  Show analyses: "yes", "no", "yes", "no", "no", 10
  Spectrogram settings: 0, max_range, 0.005, dynamic_range
  Intensity settings: 40, intensity_max + 20, "mean energy", "yes"
  Zoom: tmin-margin, tmax+margin
  Move cursor to: (tmax + tmin)*0.5

  ## Start pause window
  beginPause: "Segment finder"
    comment: "Case: 'index_row'/'number_of_rows'"
    comment: "Status: 'status$'"
    integer: "Next case:", if index_row + 1 > number_of_rows then 1 else index_row + 1 fi
  clicked = endPause: "Jump", "Save", "Quit", 1

  ## No white spaces in intervals
  interval_text$ = Get label of interval
  endeditor

  ## Next case
  next_case = if next_case > number_of_rows then 1 else next_case fi
  next_case = if next_case < 1 then 1 else next_case fi
  
  if clicked = 1
    index_row = next_case
  elsif clicked = 2
    selectObject: index
    Set numeric value: index_row, status_col$, 1
    selectObject: tg
    Save as text file: tg_folder$ + "/" + tg_filename$
    index_row = next_case
  endif

  removeObject: tg, sd

  if clicked = 3
    removeObject: index
    exitScript()
  endif
endwhile