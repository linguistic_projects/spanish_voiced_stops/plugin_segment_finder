# Copyright 2017 Rolando Muñoz

include ../../procedures/left_join.proc

tmp[1] = selected("Table")
phrases_info = Read from file: "../../preferences/info_targets.Table"
speaker_info = Read from file: "../../preferences/info_speakers.Table"

@leftJoin: tmp[1], phrases_info, "key_value", "key_value"
tmp[2] = selected("Table")
@leftJoin: tmp[2], speaker_info, "speaker", "speaker"
Rename: "index"
index = selected("Table")

removeObject: tmp[1], tmp[2], phrases_info, speaker_info
selectObject: index

#@sortTablebyColumns: "status"


