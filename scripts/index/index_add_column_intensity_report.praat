# Copyright 2017 Rolando Muñoz

include ../../procedures/config.proc

## index_table
tb_index = selected("Table")
Append column: "intensity_min"
Append column: "intensity_max"
Append column: "intensity_diff"
Append column: "intensity_mean"

@config.init: "../../preferences/logfile.txt"
sdFolderDir$ = config.init.return$["sd_folder.dir"]

for i to Object_'tb_index'.nrow
  tgName$ = Object_'tb_index'$[i, "filename"] + ".TextGrid"
  tgName_tmp$ = Object_'tb_index'$[i-1, "filename"] + ".TextGrid"
  sdName$ = Object_'tb_index'$[i, "filename"] + ".wav"
  tmin = Object_'tb_index'[i, "tmin"]
  tmax = Object_'tb_index'[i, "tmax"]  

  if tgName$ != tgName_tmp$
    nocheck removeObject: sd, intensityID
    sdDir$ = sdFolderDir$ + "/" + sdName$

    if fileReadable(sdDir$)
      sd = Read from file: sdDir$
    else
      exitScript: "'sdName$' could not be found in 'sdFolderDir$'"
    endif
    intensityID = To Intensity: 100, 0, "yes"
  endif

  selectObject: intensityID
  intensity_min = Get minimum: tmin, tmax, "Parabolic"
  intensity_max = Get maximum: tmin, tmax, "Parabolic"
  intensity_difference = intensity_max - intensity_min 
  intensity_mean = Get mean: tmin, tmax, "dB"
  
  selectObject: tb_index
  Set numeric value: i, "intensity_min", round(intensity_min)
  Set numeric value: i, "intensity_max", round(intensity_max)
  Set numeric value: i, "intensity_diff", round(intensity_difference)
  Set numeric value: i, "intensity_mean", round(intensity_mean)
endfor
nocheck removeObject: sd, intensityID

selectObject: tb_index
